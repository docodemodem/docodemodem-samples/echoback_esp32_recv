/*
 * Sample program for DocodeModem
 * receive only
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <docodemo.h>
#include <SlrModem.h>

const uint8_t CHANNEL = 0x10;   //10進で16チャネルです。通信相手と異なると通信できません。
const uint8_t DEVICE_DI = 0x10; //通信相手のIDです。
const uint8_t DEVICE_EI = 0x11; // 自分のIDです。
const uint8_t DEVICE_GI = 0x02; //グループIDです。通信相手と異なると通信できません。

DOCODEMO Dm = DOCODEMO();
SlrModem modem;
HardwareSerial UartModem(MODEM_UART_NO);

void setup()
{
  Dm.begin(); //初期化が必要です。

  //デバッグ用シリアルの初期化です
  SerialDebug.begin(115200);
  while (!SerialDebug)
    ;

  //モデム用シリアルの初期化です。通信速度とポート番号を注意してください。
  UartModem.begin(MLR_BAUDRATE, SERIAL_8N1, MODEM_UART_RX_PORT, MODEM_UART_TX_PORT);
  while (!UartModem)
    ;

  //モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  //モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  //各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存するようにしてください。
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);
}

void loop()
{

  while (1)
  {
    modem.Work(); //ループ処理。受信データを処理しています。

    if (modem.HasPacket())
    {
      const uint8_t *pData;
      uint8_t len{0};
      short rcvdata[2];
      short senddata[4];

      //受信データのポインタとサイズを取得
      modem.GetPacket(&pData, &len);

      if (len == 4)
      {
        SerialDebug.println("received");

        //受信データ取得
        memcpy(&rcvdata[0], pData, len);

        //送信データ用意
        senddata[0] = rcvdata[0];
        senddata[1] = rcvdata[1];

        //受信時のRSSI値
        modem.GetRssiLastRx(&senddata[3]);

        //環境RSSI値
        modem.GetRssiCurrentChannel(&senddata[2]);

        //受信データ出力。ここを必要な処理に変更してください。
        auto rc = modem.TransmitData((uint8_t *)senddata, 8);
        if (rc == SlrModemError::Ok)
        {
          SerialDebug.println("Echo back OK!");
        }else{
          SerialDebug.println("Echo back NG...");
        }
      }

      //受信データ開放
      modem.DeletePacket();
    }

    delay(10);
  }
}